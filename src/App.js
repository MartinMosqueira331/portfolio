import React,{useState, useRef} from 'react';
import Navbar from "./pages/Navbar";
import Fonts from "./styles/Fonts";
import {
  BrowserRouter as Router,
  Routes ,
  Route
} from "react-router-dom";
import Works from "./pages/Works";
import Home from "./pages/Home";
import Contact from "./pages/Contact";
import Themes from "./styles/theme/Theme"
import Footer from "./pages/Footer"
import {ThemeProvider} from "styled-components";
import { GlobalStyle} from './styles/App.style';
import { Canvas, useFrame } from '@react-three/fiber';
import { useLoader } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import { Suspense } from "react";
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader';
import { TextureLoader } from 'three/src/loaders/TextureLoader'
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader";

function Scene() {
  const materials = useLoader(MTLLoader, 'model/project.mtl');
  const obj = useLoader(OBJLoader, 'model/project.obj', loader => {
  materials.preload()
  loader.setMaterials(materials)
})
  const colorMap = useLoader(TextureLoader, 'model/project.png')
  const boxRef = useRef();

  useFrame(() => {
    boxRef.current.rotation.y += 0.003;
  });
  return (
    <>
      <ambientLight intensity={0.2} />
      <directionalLight />
      <mesh rotation-x={Math.PI * 0.10} rotation-y={Math.PI * 0.10} position={[0, -1, 0]}>
        <primitive ref={boxRef} object={obj} scale={[0.5, 0.6, 0.5]} />
        <meshStandardMaterial map={colorMap}/>
      </mesh>
    </>
  )
}

function App() {
  const [theme, setTheme] = useState('dark')

  return (
    <ThemeProvider theme={Themes[theme]}>
        <Router>
          <>  
              <GlobalStyle />
              <Fonts />
              <Navbar theme={theme} setTheme={setTheme}/>
              <Canvas style={{
                height: `15.7rem`,
                width: `18.7rem`,
                top: `0`,
                left: `0`,
                right: `0`,
                bottom: `0`,
                margin: 'auto',
                }}>
                <Suspense fallback={null}>
                  <Scene />
                  <OrbitControls maxDistance={10} minDistance={5}/>
                </Suspense>
              </Canvas>
              <Footer/>
            <Routes>
              <Route path='/works' element={<Works />}/>
              <Route path='/contact' element={<Contact />}/>
              <Route exact path='/' element={<Home />}/>
            </Routes>
          </>
        </Router>
    </ThemeProvider>
  );
}

export default App;
