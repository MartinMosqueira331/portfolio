import {TextF,Link,FooterDiv, FooterId} from '../styles/Footer.style'
import {DiGithubBadge,DiBitbucket} from "react-icons/di";
import { AiOutlineLinkedin } from "react-icons/ai";
import { SiHackerrank } from "react-icons/si";

const Footer = () =>{
    let urlGitHub="https://github.com/MartinMosqueira"
    let urlLinkedin="https://www.linkedin.com/in/martin-mosqueira-17a2671b5/"
    let urlBitBucket="https://bitbucket.org/MartinMosqueira331/"
    let urlHackerRank="https://www.hackerrank.com/tinchomosqueira"

    return(
        <FooterId>
            <FooterDiv>
                <Link href={urlLinkedin} target='_blank'><AiOutlineLinkedin/></Link>
                <Link href={urlBitBucket} target='_blank'><DiBitbucket/></Link>
                <Link href={urlGitHub} target='_blank'><DiGithubBadge/></Link>
                <Link href={urlHackerRank} target='_blank'><SiHackerrank/></Link>
            </FooterDiv>
            <TextF>Designed & Built by Martin Mosqueira</TextF>
        </FooterId>
    )
}

export default Footer
