import {DivM,
        DivT,
        PSub,
        DivWT,
        DivW,
        DivW2,
        DivW3,
        DivWT2,
        DivWT3,
        Button,
        Perfil,
        PerfilH,
        DivC,
        Heart,
        Span,
    } from '../styles/Home.styles'
import perfil from '../img/perfil.jpeg'
import { motion } from "framer-motion"
import { NavLink } from "react-router-dom";
import { BiRightArrow } from "react-icons/bi";

const Home = () => {

    const container ={
        hidden: { opacity: 0 },
          show: {
            opacity: 1,
            transition: {default:{duration:0.8}}
          },
    }


    return (
        <motion.div
            variants={container}
            initial="hidden"
            animate="show"
        >
            <DivM>Hello,I'm a Front-End developer based in Argentina!</DivM>
            <DivC>
                <DivT>Martin Mosqueira</DivT>
                <PSub>(Developer/Designer)</PSub>
                <Perfil src={perfil} alt="logo"></Perfil>
                <PerfilH/>
                <DivWT>About me</DivWT>
                <DivW>Martin is a Front-End developer based in Argentina with a passion for design, animation and dynamic user experiences. He likes to have fun testing new APIs creating creative designs, also interested in testing new technologies, constantly learning.
                    <br/>Solving and understanding real life problems better through the UI.
                    <br/>Interested in working on new projects with positive people.
                </DivW>
                <NavLink to="/works"><Button>Portfolio &gt;</Button></NavLink>
                <DivWT2>Bio</DivWT2>
                <DivW2>
                    <b>2015</b> <BiRightArrow/> Work as a Graphic Designer<br/>
                    <b>2016</b> <BiRightArrow/> Starting University<br/>
                    <b>2022</b> <BiRightArrow/> to Present Finishing University
                </DivW2>
                <DivWT3>I <Heart/></DivWT3>
                <DivW3>Drawing, <Span>Graphic Designer</Span>, Climb Mountains, <Span>Read Books</Span>, Cycling, Running.</DivW3> 
            </DivC>
        </motion.div>
    )
}

export default Home
