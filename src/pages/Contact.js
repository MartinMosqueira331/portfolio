import React,{useState} from 'react'
import { DivC,H2,Input,Textarea,Fields,Button} from '../styles/Contact.styles'
import { MdSend } from "react-icons/md";
import { AiOutlineWarning } from "react-icons/ai";
import { Formik,Form} from 'formik';
import ApiKey from '../api/ApiKey';
import emailjs from 'emailjs-com';

const sendEmail =(e) => {

    emailjs.send(ApiKey.SERVICE_ID, ApiKey.TEMPLATE_ID, e, ApiKey.USER_ID).then(
        result => {
            alert('Correo enviado correctamente');
        },
        error => {
            alert( 'Ocurrio un error, intente nuevamente')
            }
    )
}

const Contact = () => {
    const [FormSubmit, setFormSubmit] = useState(false);

    const container ={
        hidden: { opacity: 0 },
        show: {
            opacity: 1,
            transition: {default:{duration:0.8}}
        }
    }

    return (
        <>
            <Formik
                initialValues={{
                    email:'',
                    message: '',
                }}
                validate={(props)=>{
                    let errores={};

                    //validacion email
                    if(!props.email){
                        errores.email='Completa este campo';
                    }else if(!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(props.email)){
                        errores.email=`Incluye un signo "@" en la dirección de correo electrónico. La dirección "${props.email}" no incluye el signo "@".`;
                    }

                    //validacion Message
                    if(!props.message){
                        errores.message='Completa este campo';
                    }
                    return errores;
                }}  
                onSubmit={(props,{resetForm})=>{
                    sendEmail(props);
                    resetForm();
                    setFormSubmit(true);
                    setTimeout(()=>setFormSubmit(false),5000);
                    console.log('Formulario Enviado')
                }}
            >
                {({errors, touched})=>(
                    <DivC
                    variants={container}
                    initial="hidden"
                    animate="show"
                    >
                        <H2>Say Hello</H2>
                        <Form>
                            <Fields>
                                <Input type="email" 
                                        name="email" 
                                        placeholder="Your Email"  
                                        />
                                {touched.email && errors.email && <div style={{color: 'red'}}><AiOutlineWarning /> {errors.email}</div>}
                            </Fields>
                            <Fields>
                                <Textarea type="text" 
                                        name="message" 
                                        placeholder="Message" 
                                        />
                                {touched.message && errors.message && <div style={{color: 'red'}}><AiOutlineWarning /> {errors.message}</div>}
                            </Fields>
                            <Fields>
                                <Button type="submit"><MdSend /> Send Message</Button>
                                {FormSubmit && <p style={{color: 'green'}}>Formulario enviado con exito!</p>}
                            </Fields>
                        </Form>
                    </DivC>
                )}
                
            </Formik>
        </>
        
    )
}

export default Contact
