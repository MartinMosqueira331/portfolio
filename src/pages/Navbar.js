import React,{useState} from 'react'
import {NavLink} from "react-router-dom";
import {Header,Ul,Button,ButtonP,SNavLink,Li,ImgL,Nav} from '../styles/Navbar.style'
import { BsFillBrightnessHighFill,BsFillMoonFill } from "react-icons/bs";
import Icono from '../img/icono.png'

const variants ={
    dark: {
        background: "#6245bd",
        color: "white",
      },
      light: {
        background: "#feca7b",
        color: "#202023",
        rotate:360,
      }
  }

const Navbar = (props) => {
    const [hamburguer, sethamburguer] = useState(false);

    let urlResume="https://drive.google.com/file/d/101Q02xDfz_aXt4p-Q0BDtYiSM9sJ7kQ1/view?usp=sharing";
    const [color, setcolor] = useState(true);

    const changMode = () => {
        if(props.theme === 'light') {
            props.setTheme('dark');
            changeColor();
        }else{
           props.setTheme('light');
           changeColor();     
        }
    }

    const changeColor = () => {
        setcolor(!color);
    }

    return (
        <Header>
            <Nav>
                <Ul open={hamburguer}>
                    <NavLink to="/"><ImgL src={Icono} alt="logo"></ImgL></NavLink>
                    <Li><SNavLink activeClassName="active" to="/works">Works</SNavLink></Li>
                    <Li><SNavLink activeClassName="active" to="/contact">Contact</SNavLink></Li>
                    <Li><a href={urlResume} target="_blank"><ButtonP>Resume</ButtonP></a></Li>
                </Ul>
            </Nav>
            <NavLink to="#"><Button
            onClick={() => changMode()}
            animate={color ? "light" : "dark"}
            variants={variants}            
            ><span>{color ?  <BsFillBrightnessHighFill/>: <BsFillMoonFill/>}</span></Button></NavLink>
            {/*<div onClick={() =>sethamburguer(!hamburguer)}>{hamburguer?<Cross /> : <IconA />}</div>*/}
        </Header>
    )
}

export default Navbar
