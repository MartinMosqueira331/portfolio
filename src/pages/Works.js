import React from 'react'
import { H2,DivC,Img,Wrapper,H3Img,PImg } from '../styles/Works.style'
import PokéApi  from '../img/PokéApi.jpg'
import PythonDjango from '../img/ApiCinema.jpg'
import Sudoku from '../img/Sudoku.jpg'

const Works = () => {
    let urlPokéApi="https://bitbucket.org/MartinMosqueira331/pokemon-card/src/main/";
    let urlCinema="https://github.com/MartinMosqueira/Project_P1_Django";
    let urlSudoku="https://github.com/MartinMosqueira/Sudoku";

    const container ={
            hidden: { opacity: 0 },
            show: {
                opacity: 1,
                transition: {default:{duration:0.8}}
            }
        }
    
    return (
        <DivC
            variants={container}
            initial="hidden"
            animate="show"
        >
            <H2>Works</H2>
            <Wrapper>
                <div>
                    <a href={urlPokéApi} target="_blank"><Img src={PokéApi} alt="logo"/></a>
                    <H3Img>Pokémon Card</H3Img>
                    <PImg>A complete application based on pokémon cards, consumed through an API.</PImg>
                </div>
                <div>
                    <a href={urlCinema} target="_blank"><Img src={PythonDjango} alt="logo"/></a>
                    <H3Img>API RESTful Cinema</H3Img>
                    <PImg>Api created with all the necessary routes for the administration and management of a cinema, university project.</PImg>
                </div>
                <div>
                    <a href={urlSudoku} target="_blank"><Img src={Sudoku} alt="logo"/></a>
                    <H3Img>Sudoku</H3Img>
                    <PImg>Program that simulates the Sudoku game made at the university.</PImg>
                </div>
            </Wrapper>
        </DivC>
    )
}

export default Works