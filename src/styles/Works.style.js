import styled from "styled-components";
import { motion } from "framer-motion"
import {device } from "./Devices";

export const DivC = styled(motion.div)`
   width:39.69rem;
   height:45rem;
   margin:auto;
   position:relative;
   @media ${device.mobileS} {
      width:16.88rem;
      height:52rem;
  }
  @media only screen and (min-width:321px) and ${device.mobileM}{
    width:21rem;
    height:60.5rem;
  }
  @media only screen and (min-width:376px) and ${device.mobileL}{
    width:24rem;
    height:35rem;      
  }
`;

export const H2 = styled.h2`
  font-family: 'Inter', sans-serif;
  color: ${({theme})=>theme.textClear};
  @media ${device.mobileS} {
    font-size:1rem;
  }
  @media only screen and (min-width:321px) and ${device.mobileM}{
    font-size:1.2rem;      
  }
  @media only screen and (min-width:376px) and ${device.mobileL}{
    font-size:1.3rem;      
  }
`;

export const Wrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(2,1fr);
  @media ${device.mobileM} {
    grid-template-columns: repeat(1,1fr);
  }
`;

export const Img = styled.img`
    width:85%;
    border-radius:0.94rem;
    cursor:pointer;
    @media only screen and (min-width:321px) and ${device.mobileM}{
     width:75%;
    }
`;

export const H3Img = styled.h3`
    font-family: 'Inter', sans-serif;
    text-align:center;
    color:darkGray;
    margin-right:15%;
    @media ${device.mobileS} {
        font-size:0.9rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
      font-size:1.1rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
      font-size:1rem;      
    }
`;

export const PImg = styled.p`
    font-family: 'Inter', sans-serif;
    color: darkGray;
    justify-content: center;
    @media ${device.mobileS} {
        font-size:0.8rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
      font-size:1rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
      font-size:0.9rem;      
    }
`;