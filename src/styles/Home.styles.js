import styled from "styled-components";
import {AiFillHeart} from "react-icons/ai";
import {device } from "./Devices";

export const DivM = styled.div`
    font-family: 'Inter', sans-serif;
    text-align: center;
    background:${({theme})=>theme.bgcB};
    color: ${({theme})=>theme.textG};
    padding: 0.94rem;
    margin: 0 0 1.56rem;
    overflow: hidden;
    border-radius:0.44rem;
    width: 37.5rem;
    margin-right:auto;
    margin-left:auto;
    @media ${device.mobileS} {
        font-size:0.6rem;
        width:17rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:0.7rem;
        width:20.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.8rem;
        width:23.5rem;    
    }
`;

export const DivT = styled.h1`
    font-family: 'Inter', sans-serif;
    color: ${({theme})=>theme.textClear};
    @media ${device.mobileS} {
        font-size:1.25rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:1.30rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:1.5rem;
    }
`;

export const PSub = styled.p`
    font-family: 'Inter', sans-serif;
    color: ${({theme})=>theme.textDark};
    position:absolute;
    top:1.56rem;
    @media ${device.mobileS} {
        font-size:0.80rem;
        top:0.8rem;
  }
  @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:0.90rem;
        top:0.7rem;            
    }
  @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.90rem;
        top:0.8rem;
    }
`;

export const Perfil = styled.img`
    height: 6.88rem;
    width: 6.88rem;
    border-radius: 50%;
    object-fit: cover;
    margin-bottom: 1rem;
    border: solid ${({theme})=>theme.textDark};
    position:absolute;
    left:32.5rem;
    top:0;
    cursor:pointer;
    @media ${device.mobileS} {
        height:3rem;
        width:3rem;
        left:13.5rem;
        margin-bottom:0.5rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        height:4rem;
        width:4rem;
        left:16.5rem;
        margin-bottom:0.6rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        height:4rem;
        width:4rem;
        left:19.5rem;
        margin-bottom:0.6rem;
    }
`;

export const PerfilH = styled.div`
    height:6.88rem;
    width:6.88rem;
    border-radius:50%;
    object-fit:cover;
    margin-bottom:1rem;
    border:solid ${({theme})=>theme.textG};
    position:absolute;
    left:32.5rem;
    top:0;
    cursor:pointer;
    background-color:${({theme})=>theme.bgcC};
    opacity:0;
    transition: all 500ms ease-out;
    &:hover{
        opacity:1;
    }
    @media ${device.mobileS} {
        height:3rem;
        width:3rem;
        left:13.5rem;
        margin-bottom:0.5rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        height:4rem;
        width:4rem;
        left:16.5rem;
        margin-bottom:0.6rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        height:4rem;
        width:4rem;
        left:19.5rem;
        margin-bottom:0.6rem;
    }
`;


export const DivWT = styled.h2`
    font-family: 'Inter', sans-serif;
    text-decoration: underline 0.13rem ${({theme})=>theme.textG};
    color: ${({theme})=>theme.textClear};
    position:absolute;
    top:5.6rem;
    @media ${device.mobileS} {
       font-size:1rem;
       top:3.5rem; 
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:1.2rem;
        top:3.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:1.3rem;
        top:3.5rem;
    }
`;

export const DivW = styled.p`
    font-family: 'Inter', sans-serif;
    justify-content:center;
    position:absolute;
    top:8.13rem;
    color: ${({theme})=>theme.textDark};
    text-indent: 1.25rem;
    @media ${device.mobileS} {
       font-size:0.7rem;
       top:5.3rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:0.8rem;
        top:5.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.9rem;
        top:5.6rem;
    }
`;

export const DivC = styled.div`
   width:39.69rem;
   height:33.75rem;
   margin:auto;
   position:relative;
   @media ${device.mobileS} {
       width:16.88rem;
       height:27rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        width:21rem;
        height:28rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        width:24rem;
        height:30rem;
    }
`;

export const DivWT2 = styled(DivWT)`
    top:20rem;
    @media ${device.mobileS} {
       top:16.5rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        top:16.5rem;      
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        top:18rem;
    } 
`;

export const Heart = styled(AiFillHeart)`
    color:pink;
    position:absolute;
    top:0.19rem;
`;

export const DivW2 = styled(DivW)`
    position:absolute;
    top:22.5rem;
    text-indent:0;
    @media ${device.mobileS} {
       top:18.4rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        top:18.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        top:20rem;
    }
`;

export const DivWT3 = styled(DivWT)`
    top:28.13rem;
    @media ${device.mobileS} {
       top:22rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        top:22.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        top:24rem;
    }
`;

export const DivW3 = styled(DivW)`
    top:30.63rem;
    @media ${device.mobileS} {
       top:24rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        top:24.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        top:26rem;
    }
`;

export const Button = styled.button`
    font-family: 'Inter', sans-serddif;
    position:absolute;
    top:18.13rem;
    left: 50%;
    transform: translate(-50%, 0);
    background-color:${({theme})=>theme.textG};
    font-size:1rem;
    color:${({theme})=>theme.bgcA};
    padding-top:0.44rem;
    padding-bottom:0.44rem;
    padding-right:1.19rem;
    padding-left:1.19rem;
    border-radius:0.31rem;
    border:none;
    cursor:pointer;
    transition: all 300ms ease-out;
    &:hover{
        background-color:${({theme})=>theme.bgcA};
        color:${({theme})=>theme.textG};
        border: 0.06rem solid ${({theme})=>theme.textG};
    }
    @media ${device.mobileS} {
        font-size:0.6rem;
        padding-top:0.34rem;
        padding-bottom:0.34rem;
        padding-right:0.7rem;
        padding-left:0.7rem;
        border-radius:0.26rem;
        top:15.5rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:0.7rem;
        padding-top:0.37rem;
        padding-bottom:0.37rem;
        padding-right:0.7rem;
        padding-left:0.7rem;
        border-radius:0.28rem;
        top:15.5rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.9rem;
        padding-top:0.40rem;
        padding-bottom:0.40rem;
        padding-right:0.8rem;
        padding-left:0.8rem;
        border-radius:0.30rem;
        top:16.5rem;
    }
`;

export const Span = styled.span`
    color:${({theme})=>theme.span};
`;
