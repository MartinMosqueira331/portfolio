import styled from "styled-components";
import { motion } from "framer-motion";
import { Field} from 'formik';
import {device } from "./Devices";

export const DivC = styled(motion.div)`
    font-family: 'Inter', sans-serif;
    width: 39.69rem;
    height:25rem;
    position: relative;
    margin: auto;
    top: 2.5rem;
    @media ${device.mobileS} {
        width:16.88rem;
        height:25rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        width:21rem;
        height:27rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        width:24rem;
        height:26rem;
    }
`;

export const H2 = styled.h2`
    font-family: 'Inter', sans-serif;
    text-align: center;
    color: ${({theme})=>theme.textClear};
    @media ${device.mobileS} {
        font-size:1rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:1.2rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:1.3rem;     
    }
`;

export const Fields = styled.div`
    margin:2.5rem;
    position:relative;
`;

export const Input = styled(Field)`
    display: block;
    background:none;
    color: darkGray;
    font-size:1.13rem;
    padding: 0.63rem 0.63rem 0.63rem 0.31rem;
    width: 70%;
    border: none;
    outline: none;
    transition:0.7s ease-in;
    border-bottom: 0.13rem solid darkGray;
     &:focus{    
        border-bottom: 0.13rem solid ${({theme})=>theme.textG};
    }
    &:invalid{
       border-bottom: 0.13rem solid #ff0000;
    }
     @media ${device.mobileS} {
        font-size:0.9rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:1rem;   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:1.1rem;     
    }
`;

export const Button = styled.button`
    background:linear-gradient(490deg,#88ccca 95%,transparent 5%);
    font-size:1.06rem;
    color:${({theme})=> theme.bgcA};
    padding:0.75rem 1.5rem;
    border:none;
    transition:0.3s ease-out;
    &:hover{
        font-size:1.13rem;
        padding:0.81rem 1.56rem;
    }
    @media ${device.mobileS} {
        font-size:0.6rem;
        padding:0.50rem 0.50rem;
        &:hover{
        font-size:0.7rem;
        padding:0.55rem 0.55rem;
    }
  }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-size:0.7rem;
        padding:0.60rem 0.60rem;
        &:hover{
            font-size:0.8rem;
            padding:0.65rem 0.65rem;
        }   
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.9rem;
        padding:0.55rem 0.55rem;
        &:hover{
        font-size:1rem;
        padding:0.60rem 0.60rem;
        }
    }
`;

export const Textarea = styled.textarea`
    font-family: 'Inter', sans-serif;
    display: block;
    background:none;
    color: darkGray;
    font-size:1.13rem;
    width: 70%;
    border: none;
    outline: none;
    transition:0.7s ease-in;
    border-bottom: 0.13rem solid darkGray;
    overflow:hidden;
     &:focus{    
        border-bottom: 0.13rem solid ${({theme})=>theme.textG};
    }
    &:invalid{
       border-bottom: 0.13rem solid #ff0000;
    }
    @media ${device.mobileS} {
        font-size:0.9rem;
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
       font-size:1rem; 
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:1.1rem;     
    }
`;
