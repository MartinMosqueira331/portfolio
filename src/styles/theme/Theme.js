const DarkTheme ={
    bgcA:'#1e2128',
    bgcB:"#1C2439",
    bgcC:"rgb(136,204,202,0.5)",
    textG:'#88ccca',
    textClear:'#ccd6f6',
    textDark:'#8892b0',
    logo:"white",
    span:"#ed3581",
}

const LightTheme ={
    bgcA:'#ece2d3',
    bgcB:"#f2ebdf",
    bgcC:"rgb(161,130,98,0.5)",
    bgcMHome:"#f5eee3",
    colorMHome:"#202023",
    textG:"#a18262",
    textDark:"#45322e",
    textClear:"#604232",
    logo:"#252525",
    span:"#2596be",
}

const Themes = {
    dark:DarkTheme,
    light:LightTheme,
}

export default Themes
