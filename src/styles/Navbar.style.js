import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { motion } from "framer-motion";
import {device } from "./Devices";
/*
import { GiHamburgerMenu } from "react-icons/gi";
import { ImCross } from "react-icons/im";
*/

export const Header = styled.header`
    background-color:${({theme})=>theme.bgcB};
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0rem 10%;
    /*@media ${device.tablet} {
        padding: 0.5rem 5%;
  }*/
  `;

export const Nav = styled.nav`
    /*@media ${device.mobileL} {
       margin-left: 70%; 
    }
    @media only screen and (min-width:426px) and ${device.tablet} {
        margin-left:80%;
  }*/
`;

export const Ul = styled.ul`
    list-style: none;
    /*@media ${device.mobileL} {
        position: absolute;
        right:0.2rem;
        top: 1.6rem;
        background-color: ${({theme})=>theme.bgcB};
        border-radius:0rem 0rem 0.3rem 0.3rem ;
        display: flex;
        flex-direction: column;
        width:25%;
        align-items: left;
        display: ${({open})=>open ? "flex":"none"};
    } 
    @media only screen and (min-width:426px) and ${device.tablet} {
        position: absolute;
        right:0rem;
        top: 2.4rem;
        background-color: ${({theme})=>theme.bgcB};
        border-radius:0rem 0rem 0.5rem 0.5rem ;
        display: flex;
        flex-direction: column;
        width: 15%;
        align-items: left;
        display: ${({open})=>open ? "flex":"none"};
  }*/  
`;

export const Li = styled.li`
    display: inline-block;
    padding: 0rem 1.25rem;
    /*@media ${device.mobileL} {
        padding: 0.5rem;
    }
    @media only screen and (min-width:426px) and ${device.tablet} {
        padding:0.8rem; 
    }*/
    `;


export const SNavLink = styled(NavLink)`
    font-family: 'Inter', sans-serif;
    font-weight:500;
    font-size:1.06rem;
    color: ${({theme})=>theme.textClear};
    text-decoration: none;
    &.active{
        background-color: ${({theme})=>theme.textG};
        padding:0.56rem 0.31rem;
        color: ${({theme})=>theme.bgcA}; 
    }

    &:hover{
        text-decoration: underline;
    }

    /*@media ${device.mobileS} {
        font-weight:100;
        font-size:0.7rem;    
        &.active{
            background-color: ${({theme})=>theme.bgcB};
            border:0.12rem solid ${({theme})=>theme.textG};
            color: ${({theme})=>theme.textClear}; 
        }
        &:hover{
          text-decoration:none; 
        }
    }
    @media only screen and (min-width:321px) and ${device.mobileM}{
        font-weight:200;
        font-size:0.8rem;
        &.active{
            background-color: ${({theme})=>theme.bgcB};
            border:0.12rem solid ${({theme})=>theme.textG};
            color: ${({theme})=>theme.textClear}; 
        }
        &:hover{
          text-decoration:none; 
        }
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-weight:300;
        font-size:0.9rem;
        &.active{
            background-color: ${({theme})=>theme.bgcB};
            border:0.13rem solid ${({theme})=>theme.textG};
            color: ${({theme})=>theme.textClear}; 
        }
        &:hover{
          text-decoration:none; 
        }
    }
    @media only screen and (min-width:426px) and ${device.tablet} {
        &.active{
            background-color: ${({theme})=>theme.bgcB};
            border:0.13rem solid ${({theme})=>theme.textG};
            color: ${({theme})=>theme.textClear}; 
        }
        &:hover{
          text-decoration:none; 
        }
    }*/
`;

export const Button = styled(motion.button)`
    font-size:1.25rem;
    border:none;
    border-radius:0.38rem;
    padding:0.38rem 0.63rem;
    cursor:pointer;
    /*@media ${device.mobileM} {
        font-size:0.7rem;
        border-radius:0.2rem;
        padding:0.40rem 0.55rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.8rem;
        border-radius:0.3rem;
        padding:0.45rem 0.60rem;    
    }*/
`;

export const ButtonP = styled.button`
    font-family: 'Inter', sans-serif;
    color: ${({theme})=>theme.textClear};
    background-color: ${({theme})=>theme.bgcB};
    border:0.13rem solid ${({theme})=>theme.textClear};
    font-size:1rem;
    border-radius:0.19rem;
    cursor:pointer;
    padding:0.38rem 0.38rem;
    &:hover{
        border-color: ${({theme})=>theme.textG};
    }
    /*@media ${device.mobileM} {
        font-size:0.7rem;
        border-radius:0.20rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        font-size:0.8rem;
        border-radius:0.25rem;
    }*/
`;

export const ImgL = styled.img`
    position: relative;
    top: 0.5rem;
    width:2rem;
    height:1.7rem;
    right:1rem;  
`;

/*export const IconA = styled(GiHamburgerMenu)`
    display: none;
    color: ${({theme})=>theme.textG};
    @media ${device.mobileM} {
        display:block;
        padding:0rem 1.25rem;
        font-size:1rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
      display:block;
      padding:0rem 1.25rem;
      font-size:1.15rem;    
    }
     @media only screen and (min-width:426px) and ${device.tablet} {
        display:block;
        padding:0rem 1.25rem;
        font-size:1.25rem;
    }  
`;

export const Cross = styled(ImCross)`
    display: none;
    color: ${({theme})=>theme.textG}; 
    @media ${device.mobileM} {
        display:block;
        padding:0rem 1.25rem;
        font-size:1rem;
    }
    @media only screen and (min-width:376px) and ${device.mobileL}{
        display:block;
        padding:0rem 1.25rem;
        font-size:1.15rem;    
    }
    @media only screen and (min-width:426px) and ${device.tablet} {
        display: block;
        padding:0rem 1.25rem;
        font-size:1.25rem;
    } 
`;
*/