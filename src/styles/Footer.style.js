import styled from "styled-components";
import {device } from "./Devices";

export const TextF = styled.p`
   font-family: 'Caladea', serif;
   text-align:center;
   color:${({theme})=>theme.textClear};
   cursor:pointer;
   &:hover{
      color:${({theme})=>theme.textG};
   }
   @media ${device.mobileS} {
        font-size:0.6rem;
   }
   @media only screen and (min-width:321px) and ${device.mobileM}{
      font-size:0.7rem;     
   }
   @media only screen and (min-width:376px) and ${device.mobileL}{
      font-size:0.8rem;    
   }
`;

export const Link = styled.a`
   color:${({theme})=>theme.textClear};
   font-size:2.06rem;
   margin:1.25rem;
   &:hover{
      color:${({theme})=>theme.textG};
   }
    @media ${device.mobileS} {
        font-size:1.20rem;
        margin:0.8rem;
   }
   @media only screen and (min-width:321px) and ${device.mobileM}{
      font-size:1.50rem;
      margin:0.95rem;     
   }
   @media only screen and (min-width:376px) and ${device.mobileL}{
      font-size:1.8rem;
      margin:0.9rem;    
   }
`;

export const FooterDiv = styled.div`
   display: flex;
   justify-content: center;
`;

 export const FooterId = styled.footer`
   position: absolute;
   bottom: 0;
   width: 100%;
 `;