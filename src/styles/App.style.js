import {createGlobalStyle} from "styled-components"
import {device } from "./Devices";

export const GlobalStyle = createGlobalStyle`
    body{
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        background-color: ${({theme})=>theme.bgcA};
        position: relative;
        padding-bottom: 10em;
        min-height:100vh;
        @media ${device.mobileS} {
            padding-bottom: 6em;
        }
        @media only screen and (min-width:321px) and ${device.mobileM}{
            padding-bottom: 6.5em;
        }
        @media only screen and (min-width:376px) and ${device.mobileL}{
            padding-bottom:7em;
        }
    }
    ::selection { 
        background: ${({theme})=>theme.textDark};
        color: ${({theme})=>theme.textClear};
    }  
`;
