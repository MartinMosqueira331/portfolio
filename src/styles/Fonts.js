const Fonts = () => (
    <style>{`
      @import url('https://fonts.googleapis.com/css2?family=Caladea&family=Inter:wght@400;700&display=swap');
      `}</style>
  )
  export default Fonts
